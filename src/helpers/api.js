import axios from 'axios';
import { API } from '../constant';
import Toast from 'react-native-simple-toast';
import { AsyncStorage } from 'react-native';
import sqlite from './sqlite';

const axiosWithBaseUrl = axios.create({
	baseURL: API.HOST + ":" + API.PORT,
	headers: {
		'Accept': 'application/json',
		'Content-Type': 'application/json',
	},
});

axiosWithBaseUrl.interceptors.request.use(async (config) => {
	return AsyncStorage.getItem('access_token').then(token => {
      config.headers.Authorization = `Bearer ${token}`;
			return Promise.resolve(config);
	});
});

const signOutAsync = async () => {
	await AsyncStorage.removeItem('access-token');
	this.props.navigation.navigate('Auth');
};

const login = (loginId, password) => {
	return axiosWithBaseUrl.post("/api/authorization", {
		"UserName": loginId,
		"Password": password
	},{
		timeout: 8000
	}).then((result) => {
		console.log("result", result.data.Token);
		if (result.data.Token === undefined) {
			Toast.show(result.data.message);

			return;
		}

		return result.data;
	}, (error) => {
		console.log("error", error);
		if (!error.status) {
			Toast.show("Failed to log in. Please check your network connection.");
		} else {
			Toast.show("Error occurred.");
		}
	});
};

const getTasks = () => {
	//debug:
	sqlite.createSchema();
	// .then(queryResponse => {
	// 	console.log("***********createSchema queryResponse", queryResponse);
	// });

	//*****

	return axiosWithBaseUrl.post("/api/getTask").then(
		(response) => {
			console.log("response", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data.message;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to get task list. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};


const getWorkingDaysOfMonth = (date) => {
	//debug:
//	sqlite.createSchema();
console.log("date",(date.split(" ")[0]).split("-")[0]);


// var month = date.getMonth() + 1;
// var year = date.getFullYear();
//
// console.log(month);
// console.log(year);

	return axiosWithBaseUrl.post("/api/getWorkingDatesByMonth", {
		"year": parseInt((date.split(" ")[0]).split("-")[0]),
		"month":parseInt((date.split(" ")[0]).split("-")[1]),
	},{
		timeout: 8000
	}).then(
		(response) => {
			console.log("getWorkingDatesByMonth api", response);
			if (response.data === undefined) {
					console.log("getWorkingDatesByMonth api1");
				Toast.show("No data received");
				return;
			}
			console.log("getWorkingDatesByMonth api2",response.data);
			return response.data.message;
		},
		(error) => {
				console.log("getWorkingDatesByMonth api error", error);
			if (!error.status) {
				Toast.show("Failed to get task. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};


const getTasksByDate = (startDate) => {
	//debug:
//	sqlite.createSchema();

 console.log("starttime hahahaha",startDate);
	return axiosWithBaseUrl.post("/api/getTaskListByDate", {
		"startDate": startDate
	},{
		timeout: 8000
	}).then(
		(response) => {

			console.log("getTasksByDate api response", response.data);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data.message;
		},
		(error) => {
			console.log("getTasksByDate api",error)
			if (!error.status) {
				Toast.show("Failed to get task list. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};

const tokenDecoder  =() =>{
	return axiosWithBaseUrl.post("/api/tokenDecoder").then(
		(response) => {
			console.log("tokenDecoder response", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to get remark details. Please check your network connection.");
			} else if (error.status === 401) {
			//	signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
}

const getRemarkDetail = (TaskName,StartTime,EndTime) =>{
	return axiosWithBaseUrl.post("/api/getRemark", {
		"TaskName": TaskName,
		"StartTime": StartTime,
		"EndTime": EndTime,
		}).then(
		(response) => {
			console.log("getRemarkDetail response", response.data);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to get remark details. Please check your network connection.");
			} else if (error.status === 401) {
			//	signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};

const addRemark = (TaskName,StartTime,EndTime,Remark,ParticipantEmployeeID) =>{
	return axiosWithBaseUrl.post("/api/addRemark", {
		"TaskName": TaskName,
		"StartTime": StartTime,
		"EndTime": EndTime,
		"Remark":Remark,
		"ParticipantEmployeeID":ParticipantEmployeeID
		}).then(
		(response) => {
			console.log("addRemark response", response.data);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to get remark details. Please check your network connection.");
			} else if (error.status === 401) {
			//	signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};



const getTaskDetails = (taskId, startTime, endTime) => {
	return axiosWithBaseUrl.post("/api/getTaskDetail", {
			"TaskId": taskId,
			"StartTime": startTime,
			"EndTime": endTime
		}).then(
		(response) => {
			console.log(" getTaskDetails response", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data.message[0];
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to get task details. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};

const startEvent = (taskName, startTime, endTime) => {
	return axiosWithBaseUrl.post("/api/startEvent", {
			"TaskName": taskName,
			"StartTime": startTime,
			"EndTime": endTime
		}).then(
		(response) => {
			console.log("startEventResponse", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data[0];
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to start event. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};


const startParticipation = (taskName, startTime, endTime, employeeId) => {
	return axiosWithBaseUrl.post("/api/startParticipation", {
			"TaskName": taskName,
			"StartTime": startTime,
			"EndTime": endTime,
			"ParticipantEmployeeID": employeeId
		}).then(
		(response) => {
			console.log("startParticipationResponse data", response.data);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to start event. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};

const endParticipation = (taskName, startTime, endTime, employeeId) => {
	return axiosWithBaseUrl.post("/api/endParticipation", {
			"TaskName": taskName,
			"StartTime": startTime,
			"EndTime": endTime,
			"ParticipantEmployeeID": employeeId
		}).then(
		(response) => {
			console.log("endParticipation", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to start event. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
};

const addblockout = (taskName, startTime, endTime,BlockOutStartTime,BlockOutEndTime, employeeId) => {
	return axiosWithBaseUrl.post("/api/addblockout", {
			"TaskName": taskName,
			"StartTime": startTime,
			"EndTime": endTime,
			"BlockOutStartTime":BlockOutStartTime,
			"BlockOutEndTime":BlockOutEndTime,
			"ParticipantEmployeeID": employeeId
		}).then(
		(response) => {
			console.log("addblockout", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to add blockout. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
}


const removeblockout = (taskName, startTime, endTime,BlockOutStartTime,BlockOutEndTime, employeeId) => {
	return axiosWithBaseUrl.post("/api/removeblockout", {
			"TaskName": taskName,
			"StartTime": startTime,
			"EndTime": endTime,
			"BlockOutStartTime":BlockOutStartTime,
			"BlockOutEndTime":BlockOutEndTime,
			"ParticipantEmployeeID": employeeId
		}).then(
		(response) => {
			console.log("addblockout", response);
			if (response.data === undefined) {
				Toast.show("No data received");
				return;
			}
			return response.data;
		},
		(error) => {
			if (!error.status) {
				Toast.show("Failed to remove blockout. Please check your network connection.");
			} else if (error.status === 401) {
				signOutAsync();
			} else {
				Toast.show("Error occurred.");
			}
		}
	);
}





//**********************here are for sync localdb and server db API*************************//
const syncEvent = () =>{

	//get taskList
	// this.getTasks().then(taskList => {
	// 	console.log("taskList", taskList);
	// 	if (taskList !== undefined) {
	// 		taskList.map((task,index)=>{
	// 			//****************
	// 			this.getTaskDetails(task.TaskId, task.FromDate + " " + task.FromTime, task.ToDate + " " + task.ToTime).then(taskDetails => {
	// 				console.log("taskDetails", taskDetails);
	// 				this.setState({
	// 					cards: JSON.parse(taskDetails.Detail).map((participant, index) => {
	// 						var newParticipant = {};
	//
	// 						newParticipant.status = participant.status;
	// 						newParticipant.DisplayName = (participant.DisplayName);
	// 						newParticipant.Skill = (participant.Skill);
	// 						newParticipant.EmployeeId = participant.EmployeeId;
	//
	// 						newParticipant.id = index;
	// 						return newParticipant;
	// 					}),
	// 					isLoaded: true
	// 				});
	//
	// 			});
	// 		});
	//
	// 	}
	// });

	//for each result, get taskDetails

	//for each record, check with localdb
	//1. if started, push(merge) from localdb to live
	//2. if havnet start, push from live to localdb

}

export default {
	login,
	tokenDecoder,
	getTasks,
	getTasksByDate,
	getWorkingDaysOfMonth,
	getTaskDetails,
	startEvent,
	startParticipation,
	endParticipation,
	getRemarkDetail ,
	addRemark
}
