import React from 'react';
import * as SQLite from 'expo-sqlite'
//import { openDatabase } from 'react-native-sqlite-storage';
var db = SQLite.openDatabase('db.db');

var queryCreateSchema = //EventDetail table
'CREATE TABLE IF NOT EXISTS EventDetail( ' +
'EventId INTEGER PRIMARY KEY AUTOINCREMENT ,TaskId int, StartTime datetime,EndTime datetime,SupervisorId NVARCHAR(100),'+
'LocationPostalCode VARCHAR(100),TaskName varchar(200)' +
');'+
//ManpowerDetail table
'CREATE TABLE IF NOT EXISTS  ManpowerDetail(' +
'	ManpowerID int PRIMARY KEY AUTOINCREMENT ,' +
'	EmployeeID int,' +
'	EventId int ,' +
'	Skill varchar(max) ,' +
'	DisplayName nvarchar(100), FOREIGN KEY(EventId) REFERENCES EventDetail(EventId)'+
');' +
//ActionParticipation
'CREATE TABLE IF NOT EXISTS ActionParticipation(' +
'	ManpowerID int,' +
'	Action nvarchar(10) ,' +
'	Time datetime ,' +
'	SenderId nvarchar(100), FOREIGN KEY(ManpowerID) REFERENCES ManpowerDetail(ManpowerID) ' +
')' +
//RemarkToManpowerDetail
'CREATE TABLE IF NOT EXISTS RemarkToManpowerDetail(' +
'ManpowerID int ,' +
'Remark nvarchar(255) ,' +
'Time datetime ,' +
'CreatorId int, FOREIGN KEY(ManpowerID) REFERENCES ManpowerDetail(ManpowerID) ' +
');select * from EventDetail;';



var getTaskList = "select(select p.TaskName," +
            // " (select a.TaskName from " + constant.planning + "dbo.View_MSS_GetSkills a where a.TaskId = p.TaskId and "+
            // " DATEADD(day,DATEDIFF(day, 0, a.FromDate),CAST(a.FromTime AS DATETIME)) = p.StartTime  and "+
            // " DATEADD(day,DATEDIFF(day, 0, a.ToDate),CAST(a.ToTime AS DATETIME)) = p.EndTime  ) as TaskName, "+
            "p.EventId,p.TaskId, " +
            " Convert(date,p.StartTime) as FromDate,Convert(time,p.StartTime) as FromTime, " +
            " Convert(date,p.EndTime) as ToDate,Convert(time,p.EndTime) as ToTime " +
            ",p.EndTime,p.SupervisorId,p.LocationPostalCode,p.Status from " +
            " EventUserParticipation p " +
            " inner join ManpowerDetail b on b.EventId = p.EventId " +
            "  where b.EmployeeID = ? " +
            "   for json path " +
            "  ) as MTSData";



//example:https://aboutreact.com/example-of-sqlite-database-in-react-native/
const createSchema = ()=>{

console.log("createSchema");
  return db.transaction(
          tx => {
            console.log("createSchema12");
          //  tx.executeSql(queryCreateSchema, []);
            tx.executeSql(queryCreateSchema, [], (trans, result) => {
              console.log("***********************trans************************",trans);
              console.log("***********************result************************",result);
            }, (trans, error) => {
              console.log("***********************trans************************",trans);
              console.log("***********************error************************",error);

            });
          },
          error => {
            console.log('transaction error', error);
          },
          success => {
            console.log("transaction success", success);
          }
  );
}


const getTaskListSqlite = (EmployeeID)=>{
  return db.transaction(
          tx => {
            console.log("createSchema12");
          //  tx.executeSql(queryCreateSchema, []);
            tx.executeSql(getTaskList, [EmployeeID], (trans, result) => {
              console.log("***********************trans************************",trans);
              console.log("***********************result************************",result);
              var MTSData = result[0].MTSData;
              result = JSON.parse(MTSData);
              console.log("result", result);
              return result;
            //  return result.json(response(result, { "message": result }));
            }, (trans, error) => {
              console.log("***********************trans************************",trans);
              console.log("***********************error************************",error);

            });
          },
          error => {
            console.log('transaction error', error);
          },
          success => {
            console.log("transaction success", success);
          }
  );
}







export default{
  createSchema
}
