import normalize from './normalize'
import api from './api'

export default { normalize, api };