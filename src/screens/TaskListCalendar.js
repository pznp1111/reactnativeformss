import React from 'react';
import {
  ScrollView, StyleSheet,View, Dimensions, Platform, TouchableOpacity, Alert, AsyncStorage
} from 'react-native';
import constants from '../constant'

// Galio components
import {
  Card, Block, NavBar, Icon, Input, Text
} from 'galio-framework';
import theme from '../theme';
import helpers from '../helpers';
import Loader from './Loader';
import CalendarPicker from 'react-native-calendar-picker';
import moment from "moment";



const { width } = Dimensions.get('screen');

const withTimeout = (promiseOperation, timeout) => (...options) => Promise.race([
	promiseOperation(...options),
	new Promise((_, reject) => {
	TaskList.hideLoading();
		setTimeout(() => reject(new Error('Timeout!')), timeout)
	})
]);
const fetchWithTimeout = withTimeout(fetch, 8000);

export default class TaskListCalendar extends React.Component {
	constructor(props) {
    super(props);

    this.state = {
      filter: "",
      loading:false,
      cards: [],
      selectedStartDate: null,
      customDatesStyles:[]
    };

    this.handleFilterInputChange = this.handleFilterInputChange.bind(this);
    this.onDateChange = this.onDateChange.bind(this);
  }

  _retrieveAccessToken = async () => {
    try {
      const value = await AsyncStorage.getItem('access_token');
      if (value !== null) {
        return value;
      }
    } catch (error) {
      //console.log(error);
    }
  };

  showLoading() {
       this.setState({loading: true})
    }

  hideLoading() {
     this.setState({loading: false})
  }


getWorkingDaysOfMonthWithoutLoading(date){
  console.log(date);
  //var newDate = new Date(date.setMonth(date.getMonth()+1));
  //alert(date.toISOString());


  var newDate = moment(date).format('YYYY-MM-DD');
  //alert(newDate);
  console.log("getWorkingDaysOfMonthWithoutLoading",date.toISOString().replace("T"," ").replace("Z",""));
  //date = date.to
  helpers.api.getWorkingDaysOfMonth(newDate).then(taskList => {
    console.log("getWorkingDaysOfMonth", taskList);
    let day = moment(date).clone().startOf('month');
    let customDatesStyles1 = [];
    var tifOptions = Object.keys(taskList).map(function(key) {
        console.log("fafa",taskList[key].FromDate);
        var date1 = moment(taskList[key].FromDate);

        customDatesStyles1.push({
          date: date1.clone(),
          // Random colors
          style: {backgroundColor: '#9194c0'},
          textStyle: {color: 'black'}, // sets the font color
          containerStyle: [], // extra styling for day container
        });

      });
      this.setState({
        customDatesStyles: customDatesStyles1,
        isLoaded: true
      }
    );
      console.log("this yyyyyyy",this.state.customDatesStyles);
  });
}

  getWorkingDaysOfMonth(date){
    this.showLoading();
    var newDate = moment(date).format('YYYY-MM-DD');
    console.log("getWorkingDaysOfMonth",newDate);
		helpers.api.getWorkingDaysOfMonth(date).then(taskList => {
      console.log("getWorkingDaysOfMonth task", taskList);
      let day = moment(date).clone().startOf('month');
      let customDatesStyles1 = [];
      var tifOptions = Object.keys(taskList).map(function(key) {
          console.log("fafa",taskList[key].FromDate);
          var date1 = moment(taskList[key].FromDate);

          customDatesStyles1.push({
            date: date1.clone(),
            // Random colors
            style: {backgroundColor: '#9194c0'},
            textStyle: {color: 'black'}, // sets the font color
            containerStyle: [], // extra styling for day container
          });

        });
        this.setState({
          customDatesStyles: customDatesStyles1,
          isLoaded: true
        }
      );
        this.hideLoading();
        console.log("this yyyyyyy",this.state.customDatesStyles);
		});
      this.hideLoading();
  }

  getTasksByDate(date) {
    console.log("date",date);
    this.showLoading();
		helpers.api.getTasksByDate(date).then(taskList => {
			console.log("getTasksByDate tasklist", taskList);

			if (taskList !== undefined) {
				this.setState({
					cards: taskList.map((task, index) => {
						var cardObj = {};
						cardObj.title = task.TaskName;
						cardObj.TaskId = task.TaskId;
						cardObj.endTime = task.ToDate + " " + task.ToTime;
						cardObj.startTime = task.FromDate + " " + task.FromTime;
						cardObj.id = task.TaskId + " " + cardObj.startTime + " " + cardObj.endTime;
						cardObj.status = task.Status;
						return cardObj;
					}),
					isLoaded: true
				});
        this.hideLoading();
			} else{
				this.hideLoading();
			}

      this.getWorkingDaysOfMonth(date);
		});
  }

  componentDidMount() {


    // let today = moment();
    // let day = today.clone().startOf('month');
    // let customDatesStyles1 = [];
    // while(day.add(1, 'day').isSame(today, 'month')) {
    //   console.log("******each day:",day.toISOString());
    //   customDatesStyles1.push({
    //     date: day.clone(),
    //     // Random colors
    //     style: {backgroundColor: '#'+('#00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6)},
    //     textStyle: {color: 'black'}, // sets the font color
    //     containerStyle: [], // extra styling for day container
    //   });
    // }

    // this.setState({
    //   customDatesStyles: customDatesStyles1
    // });




    var d = new Date();
    console.log("date",d.toISOString());
    var n = (d.getTimezoneOffset() / 60) * (-1); //to fix timezone difference

    var timeInMss = new Date(Date.now()+n*60*60*1000);
  //  const timeInMss = d.setHours(d.getHours()+n);

  //var timeInMss = d.addHours(n); //.toISOString().slice(0, 19).replace('T', ' ');
  //  var timeInMss = new Date().setHours(0,0,0,0);
    timeInMss = timeInMss.toISOString().slice(0, 19).replace('T', ' ');
    console.log(timeInMss);
    //console.log("Mounted, adding listener");
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.getTasksByDate(timeInMss);

    });


   //  let today = moment();
   // let day = today.clone().startOf('month');
   // let customDatesStyles = [];
   // while(day.add(1, 'day').isSame(today, 'month')) {
   // customDatesStyles.push({
   //  date: day.clone(),
   //  // Random colors
   //  style: {backgroundColor: '#'+('#00000'+(Math.random()*(1<<24)|0).toString(16)).slice(-6)},
   //  textStyle: {color: 'black'}, // sets the font color
   //  containerStyle: [], // extra styling for day container
   // });
   // }
   //
   //
   // this.setState({
   //  customDatesStyles: customDatesStyles
   // });


  }

  componentWillUnmount() {
    //console.log("Unmounting, removing listener");
    this.focusListener.remove();
  }

  handleFilterInputChange(filterText) {
    this.setState({
			filter: filterText
    });
    //console.log(filterText);
  }

  onMonthChange(date){

  this.getWorkingDaysOfMonth(date);
   //  console.log("selected date",date.toISOString());
   //  var date1 = (date.toISOString().split("T")[0])+" 00:00:000";
   //  console.log("data1",date1);
   //
   //  this.getTasksByDate(date1);
   //
   //
   // this.setState({
   //   selectedStartDate: date,
   // });
  }


  onDateChange(date) {

    //somehow, it shows the previous date, so need add 1 days
    //;
    // console.log("selected date",date.toISOString());
    // var date1 = (date.toISOString().split("T")[0])+" 00:00:000";
    // console.log("data1",date1);
    //alert(moment(date).format('YYYY-MM-DD'));

    this.getTasksByDate(moment(date).format('YYYY-MM-DD'));


   this.setState({
     selectedStartDate: date,
   });
 }






  render() {
    //console.log("card",this.state.cards);
    const { navigation } = this.props;
    const { selectedStartDate } = this.state;
    const startDate = selectedStartDate ? selectedStartDate.toString() : '';
    let today = moment();

    var customDatesStyles = this.state.customDatesStyles;
    console.log("testh",customDatesStyles);


    return (

      <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
        <View style={styles.container}>
          <Loader
            loading={this.state.loading} />
        </View>

        <NavBar
          title="Task List"
          left={(
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                name="menu"
                family="feather"
                size={theme.SIZES.BASE}
                color={theme.COLORS.ICON}
              />
            </TouchableOpacity>
          )}
          style={Platform.OS === 'android' ? { marginTop: theme.SIZES.BASE } : null}
        />
        <Block>
        <View style={styles.container}>
          <CalendarPicker
            onDateChange={(date)=>this.onDateChange(date)}
            onMonthChange={(month)=>this.getWorkingDaysOfMonthWithoutLoading(month)}
            todayTextStyle={{fontWeight: 'bold'}}
            todayBackgroundColor={'transparent'}
            customDatesStyles={customDatesStyles}
          />
        </View>
        </Block>

         <ScrollView contentContainerStyle={styles.cards}>
            <Block flex space="between">
            {this.state.cards && this.state.cards.filter((card, id) => card.title.toUpperCase().includes(this.state.filter.toUpperCase())).map((card, id) => (
              <TouchableOpacity
                        flex
                        borderless
                        key={`card-${card.id}`}
                        onPress={() => card.supervising ? navigation.navigate('TaskSupervisor', {Task: card}) : navigation.navigate('TaskView', {Task: card})}
                        >
                <Block
                  card
                  height={65}
                  shadowColor={theme.COLORS.BLACK}
                  style={styles.card}
                >
                  <Text color={card.supervising ? theme.COLORS.RED : null}>
                  {card.title}
                  </Text>
                  <Text muted>
                  {card.startTime}
                  </Text>
                </Block>
              </TouchableOpacity>
            ))}
          </Block>
        </ScrollView>


      </Block>
    );
  }
}

const styles = StyleSheet.create({
  cards: {
    width,
    backgroundColor: theme.COLORS.WHITE,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  card: {
    backgroundColor: theme.COLORS.WHITE,
    width: width - theme.SIZES.BASE * 2,
    marginVertical: theme.SIZES.BASE * 0.875,
    padding: 12,
    elevation: theme.SIZES.BASE / 2,
  }
});
