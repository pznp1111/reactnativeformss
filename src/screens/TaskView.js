import React , { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import { API } from '../constant'
import {
  Image, StyleSheet, ScrollView, Platform, TouchableOpacity, View, AsyncStorage, TouchableHighlight, Dimensions
} from 'react-native';
import Modal from 'react-native-modal'

import Constants from 'expo-constants';
import { Table, Row, Rows } from 'react-native-table-component';
import * as Permissions from 'expo-permissions';

import Toast from 'react-native-simple-toast';
import Loader from './Loader';

import IconBadge from 'react-native-icon-badge';
import { TextInputMask } from 'react-native-masked-text'



// Galio components
import {
  Button, Block, Card, Text, Input, Icon, NavBar,
} from 'galio-framework';
import theme from '../theme';
import { BarCodeScanner } from 'expo-barcode-scanner';
import helpers from '../helpers'

const Author = props => (
  <Block row shadow middle space="between" style={styles.author}>
    <Block flex={0.25}>
      <Image source={{ uri: props.avatar }} style={styles.avatar} />
    </Block>
    <Block flex={0.7} style={styles.middle}>
      <Text style={{ fontWeight: '500' }}>{props.title}</Text>
      <Text p muted>{props.caption}</Text>
    </Block>
    <Block flex={0.5} row middle space="around">
      <Block row middle>
        <Icon name="eye" family="material-community" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 0.8} />
        <Text size={theme.SIZES.FONT * 0.7} p muted style={{ marginLeft: theme.SIZES.FONT * 0.25 }}>25.6k</Text>
      </Block>
      <Block row middle>
        <Icon name="heart-outline" family="material-community" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 0.8} />
        <Text size={theme.SIZES.FONT * 0.7} p muted style={{ marginLeft: theme.SIZES.FONT * 0.25 }}>936</Text>
      </Block>
    </Block>
  </Block>
);

Author.defaultProps = {
  author: null,
  title: null,
  caption: null,
};

Author.propsTypes = {
  author: PropTypes.string,
  title: PropTypes.string,
  caption: PropTypes.string,
};





const withTimeout = (promiseOperation, timeout) => (...options) => Promise.race([
	promiseOperation(...options),
	new Promise((_, reject) => {
		setTimeout(() => reject(new Error('Timeout!')), timeout)
	})
]);
const fetchWithTimeout = withTimeout(fetch, 8000);

export default class TaskView extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading:false, //to turn on/off loading indicator
      tableHead: ['Content', 'Created on','created by'],//for displaying remark header
      tableData:[],//for displaying remark data
      res: [],
      filterByState: "",
      filterByName:"",
      participantList: [],//for the selected participant
      cards: [],
      modalVisible: false, //for toggling the action modal
      cardIdInUse: -1, //to stored the selected card index
      token: "",
      isRegisterShown: true, //to toggle selected participant register btn
      isEndShown: true, //to toggle selected participant end btn
      isCameraOn:false, //to toggle camera for start
      isCameraOnForEnd:false,//to toggle camera for end
      hasCameraPermission: null, //for getting camera permission
      scanned: false,
      isRemarkModelVisible:false, //to toggle remark model
      participantRemark:"" ,//for participant remark input
      isBlockOutModalOn:false
    }
    this.handleFilterInputChange = this.handleFilterInputChange.bind(this);
  }



toggleBlockOutModal(){
  if(this.state.isBlockOutModelOn){
    //  this.getDetail();//refresh ui when close
      this.setState({ isBlockOutModalOn: false });
  } else{
      this.setState({ isBlockOutModalOn: true });
  }
}
  toggleBarcodeScanner(){
    console.log("toggleBarcodeScanner");
    if(this.state.isCameraOn){
        this.getDetail();//refresh ui when close
        this.setState({ isCameraOn: false });
    } else{
        this.setState({ isCameraOn: true });
    }
  }
  toggleBarcodeScannerEnd(){
    if(this.state.isCameraOnForEnd){
        this.getDetail();//refresh ui when close
        this.setState({ isCameraOnForEnd: false });
    } else{
        this.setState({ isCameraOnForEnd: true });
    }
  }


  handleFilterInputChange(filterText) {
    console.log("filter",filterText);
    this.setState({
			filterByName: filterText
    });
    //console.log(filterText);
  }


  toggleRemark(){
    console.log("toggleRemark");
    if(this.state.isRemarkModelVisible){
        this.setState({ isRemarkModelVisible: false });
    } else {
        this.getRemarkDetail();//refresh ui when close
        this.setState({ isRemarkModelVisible: true });
    }
  }

  showLoading() {
     this.setState({loading: true})
  }

  hideLoading() {
     this.setState({loading: false})
  }

  // toggleBlockOutModel(){
  //   if(this.state.isDateTimePickerModalVisible){
  //     this.setState({isDateTimePickerModalVisible:false});
  //   } else{
  //     this.setState({isDateTimePickerModalVisible:true})
  //   }
  //
  // }

  toggleModal(visible, cardIdInUse) {
    this.setState({ modalVisible: visible });
    this.setState({ cardIdInUse: cardIdInUse })
    //console.log("this.state.cardIdInUse", this.state.cardIdInUse);
    //console.log("cardIdInUse", cardIdInUse);
    var eventInfo = this.state.cards.filter(function (item) {
			return item.id == cardIdInUse;
    }).map(function({DisplayName, Skill, cardIdInUse, EmployeeId, status,realStatus}) {
			return {DisplayName, Skill, cardIdInUse, EmployeeId, status,realStatus};
    });
    //console.log("eventInfo toggle", eventInfo.length);
    if (eventInfo.length != 0) {
      if (eventInfo[0].realStatus == "Pending") {
        //console.log("*********here1**********");
        this.setState({ isRegisterShown: true });
        this.setState({ isEndShown: false });
      } else if (eventInfo[0].realStatus == "Start") {
         //console.log("*********here2**********");
         this.setState({ isRegisterShown: false });
         this.setState({ isEndShown: true });
      } else {
        //console.log("*********here3**********");
        this.setState({ isRegisterShown: false });
        this.setState({ isEndShown: false });
      }
    } else {
      this.setState({ isRegisterShown: false });
      this.setState({ isEndShown: false });
    }
	}

  componentDidMount() {
    const { navigation } = this.props;
		this.focusListener = navigation.addListener('didFocus', () => {
      this.getPermissionsAsync();
			this.getDetail();
		});
  }

  _retrieveAccessToken = async () => {
    try {
      const value = await AsyncStorage.getItem('access_token');
      if (value !== null) {
        return value;
      }
    } catch (error) {
      //console.log(error);
    }
  };



    filterByState(type){
      if(type == "Total"){
        this.setState({filterByState:""});
      } else if(type == "Attend"){
        this.setState({filterByState:"Start"});
      } else if(type == "Pending"){
        this.setState({filterByState:"Pending"});
      } else if(type == "Late"){
        this.setState({filterByState:"Late"});
      }

    }

  cameraRegisterParticipant(scannedInfo){
    //console.log("this.state.cards",this.state.cards);

    //decode scanned info here, now assume QR code only contain DisplayName
    //console.log("scannedInfo",scannedInfo);
    alert(scannedInfo);
    var DisplayName = scannedInfo;

    var eventInfo = this.state.cards.filter(function(item) {
			return item.DisplayName == DisplayName;
    }).map(function({DisplayName, Skill, id, EmployeeId, status,realStatus}) {
			return {DisplayName, Skill, id, EmployeeId, status,realStatus};
    });
    if(eventInfo.length != 0){
      const task = this.props.navigation.getParam('Task', {});
      if ((eventInfo[0].realStatus).includes("Pending") ) {
        this.registerParticipantHelperStartEvent(task, eventInfo);
      } else {
          Toast.showWithGravity('\n\n\nInvalid Tag or this person is not registered for the event.\n\n\n', Toast.SHORT,Toast.CENTER);
      }
    } else{
      Toast.showWithGravity('\n\n\nInvalid Tag or this person is not registered for the event.\n\n\n', Toast.SHORT,Toast.CENTER);
    }
  }

  cameraEndParticipant(scannedInfo){
    var DisplayName = scannedInfo;

    var eventInfo = this.state.cards.filter(function(item) {
      return item.DisplayName == DisplayName;
    }).map(function({DisplayName, Skill, id, EmployeeId, status,realStatus}) {
      return {DisplayName, Skill, id, EmployeeId, status,realStatus};
    });
    //console.log("eventInfo scanned", eventInfo);
    if(eventInfo.length != 0){
      const task = this.props.navigation.getParam('Task', {});
      if ((eventInfo[0].realStatus).includes("Start")) {
        this.endParticipantHelper(task, eventInfo);
      } else {
        Toast.showWithGravity('\n\n\nInvalid Tag or this person is not registered for the event or the event has not stareted\n\n\n', Toast.SHORT,Toast.CENTER);
      }
    } else{
      Toast.showWithGravity('\n\n\nInvalid Tag or this person is not registered for the event or the event has not stareted\n\n\n', Toast.SHORT,Toast.CENTER);
    }
  }

  registerParticipant() {
    //console.log("this.state.cardIdInUse", this.state.cardIdInUse);
    //console.log("this.state.cards", this.state.cards);
    this.showLoading();
    var cardid = this.state.cardIdInUse;
    var eventInfo = this.state.cards.filter(function(item) {
			return item.id == cardid;
    }).map(function({DisplayName, Skill, id, EmployeeId, status,realStatus}) {
			return {DisplayName, Skill, id, EmployeeId, status,realStatus};
    });
    //console.log("eventInfo", eventInfo);

    const task = this.props.navigation.getParam('Task', {});
    if ((eventInfo[0].realStatus).includes("Pending")) {
      this.registerParticipantHelperStartEvent(task, eventInfo);
      this.setState({
        modalVisible: false
      });
      this.hideLoading();
    } else {
      this.hideLoading();
    //  this.registerParticipantHelperStartParticipation(task, eventInfo);
      Toast.showWithGravity('\n\n\nThis person is not registered for the event.\n\n\n', Toast.SHORT,Toast.CENTER);
    }
  }

  endParticipant() {
    //console.log("this.state.cardIdInUse", this.state.cardIdInUse);
    //console.log("this.state.cards", this.state.cards);
    this.showLoading();
    var cardid = this.state.cardIdInUse;
    var eventInfo = this.state.cards.filter(function(item) {
      return item.id == cardid;
    }).map(function({DisplayName, Skill, id, EmployeeId, status,realStatus}) {
      return {DisplayName, Skill, id, EmployeeId, status,realStatus};
    });
    console.log("endParticipation eventInfo", eventInfo);
    const task = this.props.navigation.getParam('Task', {});
    if ((eventInfo[0].realStatus).includes("Start")) {
      this.endParticipantHelper(task, eventInfo);
      this.setState({
        modalVisible: false
      });
      this.hideLoading();
    } else {
      this.hideLoading();
      Toast.showWithGravity('\n\n\n'+'The event has not started, unable to end\n\n\n', Toast.SHORT,Toast.CENTER);
    }

  }


  blockoutEvent() {
    //console.log("this.state.cardIdInUse", this.state.cardIdInUse);
    //console.log("this.state.cards", this.state.cards);
    this.showLoading();
    var cardid = this.state.cardIdInUse;
    var eventInfo = this.state.cards.filter(function(item) {
      return item.id == cardid;
    }).map(function({DisplayName, Skill, id, EmployeeId, status,realStatus}) {
      return {DisplayName, Skill, id, EmployeeId, status,realStatus};
    });
    console.log("blockoutEvent eventInfo", eventInfo);
    const task = this.props.navigation.getParam('Task', {});
    if ((eventInfo[0].realStatus).includes("Start")) {
      this.blockoutEventHelper(task, eventInfo);
      this.setState({
        modalVisible: false
      });
      this.hideLoading();
    } else {
      this.hideLoading();
      // Toast.showWithGravity('\n\n\n'+'The event has not started, unable to end\n\n\n', Toast.SHORT,Toast.CENTER);
    }

  }

  registerParticipantHelperStartEvent(task, eventInfo) {
    console.log("registerParticipantHelperStartEvent", task);

		helpers.api.startEvent(task.title, task.startTime, task.endTime).then(startEventResponse => {
			console.log("startEvent", startEventResponse);
			this.registerParticipantHelperStartParticipation(task, eventInfo);
		});
  }

  endParticipantHelper(task, eventInfo) {
    console.log("endParticipantHelper");
    //console.log("task33", task);
    //console.log("eventInfo33", eventInfo);
    helpers.api.endParticipation(task.title, task.startTime, task.endTime, eventInfo[0].EmployeeId).then(endParticipationResponse => {
      console.log("endParticipation", endParticipationResponse)
      if (this.state.isCameraOn) {
        Toast.showWithGravity('\n\n\n' + eventInfo[0].DisplayName + ' Registered\n\n\n', Toast.SHORT,Toast.CENTER);
      } else{
        this.getDetail();
      }
    });
  }

  blockoutEventHelper(task, eventInfo){
    console.log("blockoutEventHelper");
    //console.log("task33", task);
    //console.log("eventInfo33", eventInfo);
    helpers.api.blockoutEvent(task.title, task.startTime, task.endTime, eventInfo[0].EmployeeId).then(endParticipationResponse => {
      console.log("blockoutEventHelper", endParticipationResponse)
      if (this.state.isCameraOn) {
        Toast.showWithGravity('\n\n\n' + eventInfo[0].DisplayName + ' Registered\n\n\n', Toast.SHORT,Toast.CENTER);
      } else{
        this.getDetail();
      }
    });
  }

  registerParticipantHelperStartParticipation(task, eventInfo) {
    console.log("registerParticipantHelperStartParticipation", eventInfo);
		helpers.api.startParticipation(task.title, task.startTime, task.endTime, eventInfo[0].EmployeeId).then(startParticipationResponse => {
			console.log("startParticipation", startParticipationResponse);
			if (this.state.isCameraOnForEnd) {
				Toast.showWithGravity('\n\n\n' + eventInfo[0].DisplayName + ' Registered\n\n\n', Toast.SHORT,Toast.CENTER);
			} else{
				this.getDetail();
			}
		});
  }

  getPermissionsAsync = async () => {
    console.log("getpermission");
    const permission = await Permissions.getAsync(Permissions.CAMERA);
        if (permission.status !== "granted") {
          const { status } = await Permissions.askAsync(Permissions.CAMERA);
          console.log("getpermission1",status.toString());
          const newPermission = await Permissions.askAsync(Permissions.CAMERA_ROLL);

          if (newPermission.status !== "granted") {
          alert("unable to acquire camera permission");
        } else {
            this.setState({ hasCameraPermission: status === 'granted' });
        }
      }
  }



  addRemark(){
    const task = this.props.navigation.getParam('Task', {});
    var token = this.state.token;
    var remark = this.state.participantRemark;
    var cardIdInUse = this.state.cardIdInUse;
    console.log("addRemark",JSON.stringify(task));
    var eventInfo = this.state.cards.filter(function (item) {
      return item.id == cardIdInUse;
    }).map(function({DisplayName, Skill, cardIdInUse, EmployeeId, status,realStatus}) {
      return {DisplayName, Skill, cardIdInUse, EmployeeId, status,realStatus};
    });

    //check for remark:1. not empty, not more than 255
    if(remark.trim() == ""){
      Alert.alert("Remark cannot be empty");
      return;
    }

    if(remark.length >=255){
      Alert.alert("Remark cannot be more than 255 characters");
      return;
    }

    console.log("getRemarkDetail",JSON.stringify(task));
    helpers.api.addRemark(task.title, task.startTime, task.endTime,remark,eventInfo[0].EmployeeId).then(remarkDetail => {
        console.log("remarkDetail", JSON.stringify(remarkDetail));
        this.getRemarkDetail();
        this.setState({
            participantRemark: ""
        });
    });
  }

  getRemarkDetail(){
    	const task = this.props.navigation.getParam('Task', {});
      console.log("getRemarkDetail",JSON.stringify(task));
      helpers.api.getRemarkDetail(task.title, task.startTime, task.endTime).then(remarkDetail => {
          console.log("remarkDetail", JSON.stringify(remarkDetail));
          this.setState({
            tableData: (remarkDetail.message).map((remarkContent, index) => {
              var content =[];//var createdOn = []; var createdBy = [];
              content.push(remarkContent.Remark);
              var timeparse = (remarkContent.Time).split("T");
              content.push(timeparse[0] + " "+ timeparse[1].substring(0,8));
              content.push(remarkContent.CreatorName);
              return content;
            }),
            isLoaded: true
          });
      });
  }



  getDetail() {
    console.log("**********************************************************");
		const task = this.props.navigation.getParam('Task', {});

    console.log("task",task);
    var t = task.startTime.split(/[- :]/);
    var taskStartTime = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
    t = (task.endTime).split(/[- :]/);
    var taskEndTime =new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
    var currentTime = new Date();


    this.showLoading();
		helpers.api.getTaskDetails(task.TaskId, task.startTime, task.endTime).then(taskDetails => {
			console.log("taskDetails", taskDetails);
			this.setState({
				cards: JSON.parse(taskDetails.Detail).map((participant, index) => {
					var newParticipant = {};
          newParticipant.realStatus = participant.status;
					newParticipant.status = participant.status;
					newParticipant.DisplayName = (participant.DisplayName);
					newParticipant.Skill = (participant.Skill);
					newParticipant.EmployeeId = participant.EmployeeId;
					newParticipant.id = index;

          if(participant.status == "End"){
            newParticipant.status = "Start and ended";
          }
          if(currentTime >taskStartTime ){
              newParticipant.status =   newParticipant.status + " Late";
          }



					return newParticipant;
				}),
				isLoaded: true
			});
        this.hideLoading();
		});
	}

  async getCoordinates(query) {
   this.setState({
     loading: true
   });
   let coords = await this.search(query);
   console.log('coords', coords)

   setTimeout(() => {
     this.setState({
       loading: false,
       address: coords.results[0].formatted_address
     });
   }, 2500);
 }

	render() {
    const task = this.props.navigation.getParam('Task', {});
    console.log("task", task);
    const state = this.state;
    const participantList = this.state.participantList;
    console.log("lalala cards", this.state.cards);
    //console.log("participantList", participantList);

		const nameToDisplay = (this.state.cards.length > 0 && this.state.cardIdInUse !== -1) ?
														this.state.cards.filter((card, id) => card.id === this.state.cardIdInUse)[0].DisplayName :
														null;

		const skillToDisplay = (this.state.cards.length > 0 && this.state.cardIdInUse !== -1) ?
														this.state.cards.filter((card, id) => card.id === this.state.cardIdInUse)[0].Skill :
														null;
    const { hasCameraPermission, scanned } = this.state;


    var participantStart = this.state.cards.filter(function (item) {
			return item.realStatus.includes("Start");
    }).map(function({DisplayName, Skill, cardIdInUse, EmployeeId, status}) {
			return {DisplayName, Skill, cardIdInUse, EmployeeId, status};
    });
    var participantPending = this.state.cards.filter(function (item) {
      return item.realStatus == "Pending";
    }).map(function({DisplayName, Skill, cardIdInUse, EmployeeId, status}) {
      return {DisplayName, Skill, cardIdInUse, EmployeeId, status};
    });

    var participantEnd = this.state.cards.filter(function (item) {
      return item.realStatus == "Pending Late";
    }).map(function({DisplayName, Skill, cardIdInUse, EmployeeId, status}) {
      return {DisplayName, Skill, cardIdInUse, EmployeeId, status};
    });

    //var summary = "Total:" + this.state.cards.length + "    Started:"+participantStart.length+"    Ended:"+participantEnd.length+"    Pending:"+participantPending.length;


    var totalNumber = this.state.cards.length;
    var totalAttended = participantStart.length;
    var totalLate = participantEnd.length;
    var totalPending = participantPending.length;


    var t = task.startTime.split(/[- :]/);
    var taskStartTime = new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
    t = (task.endTime).split(/[- :]/);
    var taskEndTime =new Date(Date.UTC(t[0], t[1]-1, t[2], t[3], t[4], t[5]));
    var currentTime = new Date();
    console.log("task",task);
    console.log("startTime",taskStartTime);
    console.log("endTime",taskEndTime);
    console.log("currentTime",currentTime);
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;
    var one_hour=1000*60*60;
  // // Convert both dates to milliseconds
  // var date1_ms = date1.getTime();
  // var date2_ms = date2.getTime();
  //
  // // Calculate the difference in milliseconds
  // var difference_ms = date2_ms - date1_ms;
  //
  // // Convert back to days and return
  // return Math.round(difference_ms/one_day);

    var d = new Date(Math.abs(currentTime.getTime() - taskStartTime.getTime()));
    var ed = new Date(Math.abs(currentTime.getTime() - taskEndTime.getTime()));
    console.log( d.getUTCMinutes() + ':' + d.getUTCSeconds() );
    var timeInSmmmary = (d/one_day).toFixed(2) ;
    var timeInSmmmaryHour = (d/one_hour).toFixed(2);


    var timeInSmmmaryed = (ed/one_day).toFixed(2) ;
    var timeInSmmmaryHoured = (ed/one_hour).toFixed(2);

    if(timeInSmmmary > 1){
      if(taskStartTime > currentTime){
        timeInSmmmary = timeInSmmmary + "days to start";
      }

      if(taskStartTime < currentTime  && taskEndTime > currentTime){
        timeInSmmmary = "started "+ timeInSmmmary +"days ago";
      }

      if(taskEndTime < currentTime){
        timeInSmmmary = "ended " + timeInSmmmaryed +" days ago";
      }
    } else{
      if(taskStartTime > currentTime){
        timeInSmmmaryHour = timeInSmmmaryHour + "days to start";
      }

      if(taskStartTime < currentTime  && taskEndTime > currentTime){
        timeInSmmmaryHour = "started "+ timeInSmmmaryHour +"days ago";
      }

      if(taskEndTime < currentTime){
        timeInSmmmaryHour = "ended" + timeInSmmmaryHoured+" days ago";
      }
      timeInSmmmary = timeInSmmmaryHour;
    }



		return (
      <Block safe flex>
        <View style={styles.container}>
          <Loader
            loading={this.state.loading} />
        </View>

				<NavBar
					title={task.title}
					titleStyle={{ alignSelf: 'flex-start' }}
					leftIconColor={theme.COLORS.MUTED}
					left={
						(
							<TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
								<Icon
								name="menu"
								family="feather"
								size={theme.SIZES.BASE}
								color={theme.COLORS.ICON}
								/>
							</TouchableOpacity>
						)
					}
					style={Platform.OS === 'android' ? { marginTop: theme.SIZES.BASE } : null}
					right={[
						<Button
							key="right-options"
							color="transparent"
							style={styles.button}
							onPress={() => this.toggleBarcodeScanner()}
						>
							<Icon size={theme.SIZES.BASE * 1.0625} name="qrcode" family="font-awesome" color={theme.COLORS.MUTED} />
						</Button>,
						<Button
							key="right-search"
							color="transparent"
							style={styles.button}
							onPress={() => this.toggleBarcodeScannerEnd()}
						>
							<Icon size={theme.SIZES.BASE * 1.0625} name="barcode" family="font-awesome" color={theme.COLORS.MUTED} />
						</Button>,
					]}
				/>

      <Block
        card
        height={180}
        shadowColor={theme.COLORS.BLACK}
        style={styles.card}
        backgroundColor='white'
      >


<View style={{flex: 10, flexDirection: 'row', marginLeft:'5%'}}>
        <View
          style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',width:80,borderRadius :5}}
          backgroundColor = {this.state.filterByState == ""?'#CDCDCD':'transparent'}
          flexDirection = 'column'
          >
          <View>
            <IconBadge
              MainElement={
                <Icon name="clipboard-notes" family="foundation" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 2.8}
                  onPress={() => this.filterByState("Total")}
                  />
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{totalNumber}</Text>
              }
              IconBadgeStyle={
                {width:20,
                height:20,
                backgroundColor: 'grey'}
              }

              />
          </View>
          <View>
            <Text>Total</Text>
          </View>
        </View>

        <View style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',marginLeft:10,width:80,borderRadius :5}}
          backgroundColor = {this.state.filterByState == "Start"?'#CDCDCD':'transparent'}
          flexDirection = 'column'
          >
          <View>
            <IconBadge styles = {{marginLeft:120}}
              MainElement={
                <Icon name="account-multiple-check" family="material-community" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 2.8}
                  onPress={() => this.filterByState("Attend")}
                  />
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{totalAttended}</Text>
              }
              IconBadgeStyle={
                {width:20,
                height:20,
                backgroundColor: 'green'}
              }
              />
          </View>
          <View>
            <Text >Attend</Text>
          </View>
        </View>

        <View
          style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',marginLeft:10,width:80,borderRadius :5}}
           backgroundColor = {this.state.filterByState == "Pending"?'#CDCDCD':'transparent'}
           flexDirection = 'column'
           >
           <View>
            <IconBadge styles = {{marginLeft:120}}
              MainElement={
                <Icon name="account-question" family="material-community" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 2.8}
                  onPress={() => this.filterByState("Pending")}
                  />
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{totalPending}</Text>
              }
              IconBadgeStyle={
                {width:20,
                height:20,
                backgroundColor: 'orange'}
              }
              />
          </View>

          <View>
            <Text >Pending</Text>
          </View>
        </View>

        <View
          style={{flexDirection: 'row',alignItems: 'center',justifyContent: 'center',marginLeft:5,width:80,borderRadius :5}}
          backgroundColor = {this.state.filterByState == "Late"?'#CDCDCD':'transparent'}
          flexDirection = 'column'
          >
          <View>
            <IconBadge styles = {{marginLeft:120}}
              MainElement={
                <Icon name="account-clock" family="material-community" color={theme.COLORS.MUTED} size={theme.SIZES.FONT * 2.8}
                  onPress={() => this.filterByState("Late")}
                  />
              }
              BadgeElement={
                <Text style={{color:'#FFFFFF'}}>{totalNumber}</Text>
              }
              IconBadgeStyle={
                {width:20,
                height:20,
                backgroundColor: 'red'}
              }
              />
          </View>
          <View>
            <Text >Late</Text>
          </View>
        </View>

</View>
<Block style = {{alignItems: 'center',marginBottom:0}}><Text muted  >
  {timeInSmmmary}
</Text></Block>
<Input style={styles.cardinside}  onChangeText={this.handleFilterInputChange} right icon='search' family='font-awesome'/>

      </Block>
				<Block flex style={styles.news}>
          <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
            <ScrollView contentContainerStyle={styles.cards}>
                <Block flex space="between">
                {this.state.cards && this.state.cards.filter((card, id) =>   (card.DisplayName.toUpperCase().includes(this.state.filterByName.toUpperCase())) &&(card.status.toUpperCase().includes(this.state.filterByState.toUpperCase()))   ).map((card, id) => (
                  <TouchableOpacity
                            flex
                            borderless
                            key={`card-${card.id}`}
                            onPress={() => this.toggleModal(!this.state.modalVisible, id)}
                            >
                    <Block
                      card
                      height={65}
                      shadowColor={theme.COLORS.BLACK}
                      style={styles.card}
                      backgroundColor={card.realStatus == 'Pending' ? card.realStatus == 'Start'?'red':'green':'yellow'}
                    >
                      <Text color={card.supervising ? theme.COLORS.RED : null}>
												{card.DisplayName}
                      </Text>
                      <Text muted>
												{card.Skill + " " + card.realStatus}
                      </Text>
                    </Block>
                  </TouchableOpacity>
                ))}
              </Block>
            </ScrollView>
          </Block>
				</Block>



        <Modal isVisible = {this.state.isCameraOn}>
            <Block >
                {
                  this.state.isCameraOn ?
                  <BarCodeScanner
                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                    style={StyleSheet.absoluteFillObject} height={300}
                  />
                :null
                }
            </Block>
              {scanned && (
                  <Button round color="info" style ={styles.bottomSecondView}
                    title={'Tap to Scan Again'}
                    onPress={() => this.setState({ scanned: false })}
                  > Continue </Button>
                )}
                <View style={styles.containerMain}>
                     <Button round style={styles.bottomView} color={theme.COLORS.GREY}  onPress={() => this.toggleBarcodeScanner()}>Close</Button>
                </View>
        </Modal>

        <Modal isVisible = {this.state.isCameraOnForEnd}>
            <Block >
                {
                  this.state.isCameraOnForEnd ?
                  <BarCodeScanner
                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScannedEnd}
                    style={StyleSheet.absoluteFillObject} height={300}
                  />
                :null
                }
            </Block>
              {scanned && (
                  <Button round color="info" style ={styles.bottomSecondView}
                    title={'Tap to Scan Again'}
                    onPress={() => this.setState({ scanned: false })}
                  > Continue </Button>
                )}
                <View style={styles.containerMain}>
                     <Button round style={styles.bottomView} color={theme.COLORS.GREY}  onPress={() => this.toggleBarcodeScannerEnd()}>Close</Button>
                </View>
        </Modal>




        <Modal isVisible = {this.state.isRemarkModelVisible}>
          <Block style = {styles.containerRemark}>


            <ScrollView  style={{height:'40%',marginTop:'3%'}}>
              <View style={{width:'94%',marginLeft:'3%'}}>
                      <Table borderStyle={{borderWidth: 2, borderColor: '#c8e1ff'}}>
                        <Row data={this.state.tableHead} style={styles.head} textStyle={styles.text}/>
                        <Rows data={this.state.tableData} textStyle={styles.head}/>
                      </Table>
              </View>
            </ScrollView>


            <View style={{
				flex: 1,
				flexDirection: 'column',
				justifyContent: 'space-between',

			  }}>
				<View style = {{marginBottom: 5,marginLeft:'5%',width:'90%'}}>
					<Input  placeholder="key in your remark" value = {this.state.participantRemark}   onChangeText={(participantRemark) => this.setState({participantRemark})}/>
				</View>

				<View style = {{marginBottom: 5}}>
					<Button round  color={theme.COLORS.RED} onPress={() => this.addRemark()}>Add Remark</Button>
				</View>

				<View style = {{marginBottom: 5}}>
					<Button round  color={theme.COLORS.GREY}  onPress={() => this.toggleRemark()}>Close</Button>
				</View>
			</View>
          </Block>

        </Modal>



		<Modal isVisible={this.state.modalVisible}>
			<Block style={ styles.actionContainer }>
				<Block>
					<Text>
					Name: {nameToDisplay}
					</Text>
					<Text>
					Skills: {skillToDisplay}
					</Text>
				</Block>
				<Block style={ styles.buttonContainer }>
					{
						this.state.isRegisterShown ?
							<Button round style={styles.actionButton} ccolor="#50C7C7" onPress={() => this.registerParticipant()}>
							Register
							</Button>
							: null
					}
					{
						this.state.isEndShown ?
							<Button round style={styles.actionButton} ccolor="#50C7C7" onPress={() => this.endParticipant()}>
							End
							</Button>
							: null
					}
					{
						(task.status.indexOf("Start")!=-1)?
							<Button round style={styles.actionButton} ccolor="#50C7C7"
							onPress={() => this.toggleRemark()}>
								Remark
							</Button>
						:null
					}



          {
            this.state.isBlockOutModalOn ?

            <View style = { styles.containerBlockout }>
              <View >
                <Text>
                BlockOut Start Time:
                </Text>
              </View>
              <TextInputMask
                type={'datetime'}
                options={{
                  format: 'YYYY-MM-DD HH:mm:ss'
                }}
                value={task.startTime}
                onChangeText={text => {
                }}
              />
              <View>
                <Text>
                BlockOut End Time:
                </Text>
              </View>
              <TextInputMask
                type={'datetime'}
                options={{
                  format: 'YYYY-MM-DD HH:mm:ss'
                }}
                value={task.endTime}
                onChangeText={text => {

                }}
              />
              <Button round style={styles.actionButton} color={theme.COLORS.GREY} >Save BlockOut</Button>

            </View>





            :null

          }



          <Button round style={styles.actionButton} color={theme.COLORS.GREY} onPress={() => this.toggleBlockOutModal()}>BlockOut</Button>
					<Button round style={styles.actionButton} color={theme.COLORS.GREY} onPress={() => this.toggleModal(!this.state.modalVisible, -1)}>
				 Close
					</Button>
				</Block>
			</Block>
		</Modal>


		  </Block>
    )
	}

  handleBarCodeScanned = ({ type, data }) => {
  this.setState({ scanned: true });
//  alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  this.cameraRegisterParticipant(data);
  };

  handleBarCodeScannedEnd = ({ type, data }) => {
  this.setState({ scanned: true });
//  alert(`Bar code with type ${type} and data ${data} has been scanned!`);
  this.cameraEndParticipant(data);
  };
}

const styles = StyleSheet.create({
  container1: {
      backgroundColor: '#CCCCCC',
      height: Dimensions.get('window').height,
      padding: 15,
      display: 'flex',
      alignItems: 'flex-start',
      width: '100%',
      paddingTop: 50
    },
  actionContainer: {
    padding: 15,
		backgroundColor: theme.COLORS.WHITE
  },

  containerMain: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerBlockout:{
    backgroundColor: '#f5e042' // Set your own custom Color
  },
  containerRemark:{
    backgroundColor:theme.COLORS.WHITE,
    width:'100%',
    height:'90%'
  },
  bottomView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 0, //Here is the trick
  },


  bottomSecondView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 60, //Here is the trick
  },

  bottomThirdView: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', //Here is the trick
    bottom: 110, //Here is the trick
  },

  textStyle: {
    color: '#fff',
    fontSize: 18,
  },

	buttonContainer: {
		alignItems: 'center'
	},
	closeButton: {
    width: '25%',
	},
	actionButton: {
		width: '50%',
		marginTop:5

	},
  news: {
    marginTop: theme.SIZES.BASE / 2,
    paddingBottom: theme.SIZES.BASE / 2,
    justifyContent: 'flex-start',
    paddingHorizontal: theme.SIZES.BASE,
  },
  bottom: {
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 36
  },
  button: {
    width: theme.SIZES.BASE * 2,
    borderColor: 'transparent',
  },
  author: {
    position: 'absolute',
    right: theme.SIZES.BASE,
    left: theme.SIZES.BASE,
    bottom: Constants.statusBarHeight,
    backgroundColor: theme.COLORS.WHITE,
    shadowOffset: { width: 0, height: 4 },
    shadowOpacity: 0.3,
    elevation: theme.SIZES.BASE / 2,
  },
  card: {
    marginVertical: theme.SIZES.BASE * 0.125,
    padding: 12,
    elevation: theme.SIZES.BASE / 2,
  },
  cardinside: {
    marginVertical: theme.SIZES.BASE * 0.125,
    padding: 12,
    elevation: theme.SIZES.BASE / 2,
    marginBottom:-5
  }

});
