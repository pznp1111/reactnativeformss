import React from 'react';
import {
  Dimensions, KeyboardAvoidingView, StyleSheet, Platform, AsyncStorage, View, StatusBar, ActivityIndicator
} from 'react-native';
import Modal from 'react-native-modal';
import { SafeAreaView } from 'react-native-safe-area-context';

// galio component
import {
  Block, Button, Input, NavBar, Text
} from 'galio-framework';
import Toast from 'react-native-simple-toast';
import theme from '../theme';
import constants from '../constant';
import helpers from '../helpers';

const { height, width } = Dimensions.get('window');
class Login extends React.Component {
  state = {
    loginId: '',
    password: '',
		isLoggingIn: false,
		configModalVisible: false,
		isLoadingConfigModal: false,
		isSavingConfig: false,
		serverAddress: '',
		serverPort: ''
  }

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  }

  _storeData = async (token,Detail) => {
    console.log("_Detail", JSON.stringify(Detail));
    try {
      await AsyncStorage.setItem('access_token', token);
      await AsyncStorage.setItem('Detail',JSON.stringify(Detail));

      this.setState({
				password: "",
				isLoggingIn: false
      });
      this.props.navigation.navigate("App");
    } catch (error) {
      // Error saving data
      console.log(error);

      this.setState({
				password: "",
				isLoggingIn: false
      });
    }
  };

	loadConfigModal = async () => {
		this.setState({
			isLoadingConfigModal: true,
			configModalVisible: true
		});
    const serverAddress = await AsyncStorage.getItem('serverAddress');
		const serverPort = await AsyncStorage.getItem('serverPort');
		this.setState({
			isLoadingConfigModal: false,
			serverAddress: serverAddress,
			serverPort: serverPort
		});
	};

	closeConfigModal = async () => {
		if (this.state.isSavingConfig) return;
		this.setState({
			configModalVisible: false
		});
	};

  handleLogin = () => {
    console.log("state",this.state);

    if (this.state.loginId == ""){
			Toast.show("Login ID cannot be empty.");
      return;
    }
		if (this.state.password == ""){
			Toast.show("Password cannot be empty.");
      return;
    }
    var loginId = this.state.loginId;
    var password = this.state.password;
		this.setState({
			isLoggingIn: true
		});

		helpers.api.login(loginId, password).then((response) => {
			this.setState({
				password: "",
				isLoggingIn: false
      });
      console.log("TokenTokenTokenToken",response);

			if (response.Token !== undefined  && response.Detail != undefined) {
				this._storeData(response.Token,response.Detail);
        global.Detail = response.Detail;
			}
      // helpers.api.tokenDecoder().then(response => {
      //   console.log("tokenDecoder fdafafa", response.message.UserName);
      //   global.userName = response.message.UserName;
      // });
		});
  }

	saveServerInformation = async () => {
		this.setState({
			isSavingConfig: true
		});

		console.log("Setting server info to: " + this.state.serverAddress + ":" + this.state.serverPort);

		await AsyncStorage.setItem('serverAddress', this.state.serverAddress);
		await AsyncStorage.setItem('serverPort', this.state.serverPort);

		this.setState({
			isSavingConfig: false
		});
		Toast.show("Settings updated.");
		this.closeConfigModal();
	}

  handleChange = (name, value) => {
    this.setState({ [name]: value });
  }

  render() {
    const { navigation } = this.props;
    const { loginId, password, serverAddress, serverPort } = this.state;

    return (
				<Block flex style={{ backgroundColor: theme.COLORS.WHITE }}>
					<Modal isVisible={this.state.configModalVisible} onBackdropPress={this.closeConfigModal}>
						<View style={styles.modalContent}>
							{
								this.state.isLoadingConfigModal ?
									<ActivityIndicator /> :
									<Block>
										<Text h5 bold center>
										Server Information
										</Text>
										<Input
											type="default"
											placeholder="e.g. 127.0.0.1"
											label="Host"
											autoCapitalize="none"
											onChangeText={text => this.handleChange('serverAddress', text)}
											value={serverAddress}
										/>
										<Input
											type="default"
											placeholder="e.g. 80"
											label="Port"
											autoCapitalize="none"
											onChangeText={text => this.handleChange('serverPort', text)}
											value={serverPort}
										/>
										<Block style={{justifyContent: 'space-around', flexDirection: 'row'}}>
											<Button
												loading={this.state.isSavingConfig}
												onPress={this.saveServerInformation}
												style={{width: '40%'}}
												color='info'
											>
											Save
											</Button>
											<Button
												onPress={this.closeConfigModal}
												style={{width: '40%'}}
												color={theme.COLORS.GREY}
												disabled={this.state.isSavingConfig}
											>
											Close
											</Button>
										</Block>
									</Block>
							}
						</View>
					</Modal>
					<SafeAreaView style={styles.container}>
						<Block flex={0.5} center>
							<Text center size={theme.SIZES.FONT * 1.5} style={{ paddingHorizontal: theme.SIZES.BASE * 2.3 }}>
								Manpower Tracking System
							</Text>
							<Text muted center size={theme.SIZES.FONT * 0.875} style={{ paddingHorizontal: theme.SIZES.BASE * 2.3 }}>
								Please sign in to view your tasks.
							</Text>
						</Block>

						<Block flex row space="between" style={{ marginVertical: theme.SIZES.BASE * 1.875 }}>
							<Block flex middle center>
								<Button
									disabled
									round
									onlyIcon
									iconSize={theme.SIZES.BASE * 1.625}
									icon="account-circle"
									iconFamily="MaterialIcons"
									color={theme.COLORS.TWITTER}
									shadowColor={theme.COLORS.TWITTER}
									iconColor={theme.COLORS.WHITE}
									style={styles.social}
								/>
							</Block>
						</Block>
						<Block flex={2} center space="evenly">
							<Block flex={2}>
								<Input
									rounded
									type="default"
									placeholder="Login ID"
									autoCapitalize="none"
									style={{ width: width * 0.9 }}
									onChangeText={text => this.handleChange('loginId', text)}
									value={loginId}
								/>
								<Input
									rounded
									password
									viewPass
									placeholder="Password"
									style={{ width: width * 0.9 }}
									onChangeText={text => this.handleChange('password', text)}
									value={password}
									returnKeyType="go"
									onSubmitEditing={this.handleLogin}
								/>
								<Button
									round
									color="error"
									onPress={this.handleLogin}
									loading={this.state.isLoggingIn}>
									Sign in
								</Button>
							</Block>
						</Block>
					</SafeAreaView>
					<Block row center space="between" style={{ marginVertical: theme.SIZES.BASE * 1.875 }}>
						<Block flex middle center>
							<Button
								round
								onlyIcon
								iconSize={theme.SIZES.BASE * 1.625}
								icon="settings"
								iconFamily="feather"
								color={theme.COLORS.GREY}
								shadowColor={theme.COLORS.GREY}
								iconColor={theme.COLORS.WHITE}
								style={styles.social}
								onPress={this.loadConfigModal}
							/>
						</Block>
					</Block>
				</Block>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  social: {
    width: theme.SIZES.BASE * 3.5,
    height: theme.SIZES.BASE * 3.5,
    justifyContent: 'center',
  },
	modalContent: {
    backgroundColor: 'white',
    padding: 15,
    justifyContent: 'center',
    alignItems: 'stretch',
    borderRadius: 4,
    borderColor: 'rgba(0, 0, 0, 0.1)',
  },
});

export default Login;
