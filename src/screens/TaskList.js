import React from 'react';
import {
  ScrollView, StyleSheet,View, Dimensions, Platform, TouchableOpacity, Alert, AsyncStorage
} from 'react-native';
import constants from '../constant'

// Galio components
import {
  Card, Block, NavBar, Icon, Input, Text
} from 'galio-framework';
import theme from '../theme';
import helpers from '../helpers';
import Loader from './Loader';


const { width } = Dimensions.get('screen');

const withTimeout = (promiseOperation, timeout) => (...options) => Promise.race([
	promiseOperation(...options),
	new Promise((_, reject) => {	
	TaskList.hideLoading();
		setTimeout(() => reject(new Error('Timeout!')), timeout)
	})
]);
const fetchWithTimeout = withTimeout(fetch, 8000);

export default class TaskList extends React.Component {
	constructor(props) {
    super(props);

    this.state = {
      filter: "",
      loading:false,
      cards: []
    };

    this.handleFilterInputChange = this.handleFilterInputChange.bind(this);
  }

  _retrieveAccessToken = async () => {
    try {
      const value = await AsyncStorage.getItem('access_token');
      if (value !== null) {
        return value;
      }
    } catch (error) {
      //console.log(error);
    }
  };

  showLoading() {
       this.setState({loading: true})
    }

  hideLoading() {
     this.setState({loading: false})
  }

  getTasks() {
    this.showLoading();
		helpers.api.getTasks().then(taskList => {
			console.log("taskList", taskList);
			if (taskList !== undefined) {
				this.setState({
					cards: taskList.map((task, index) => {
						var cardObj = {};
						cardObj.title = task.TaskName;
						cardObj.TaskId = task.TaskId;
						cardObj.endTime = task.ToDate + " " + task.ToTime;
						cardObj.startTime = task.FromDate + " " + task.FromTime;
						cardObj.id = task.TaskId + " " + cardObj.startTime + " " + cardObj.endTime;
						cardObj.status = task.Status;
						return cardObj;
					}),
					isLoaded: true
				});
        this.hideLoading();
			} else{
				this.hideLoading();
			}
		});
  }

  componentDidMount() {
    //console.log("Mounted, adding listener");
    const { navigation } = this.props;
    this.focusListener = navigation.addListener('didFocus', () => {
      this.getTasks();
    });
  }

  componentWillUnmount() {
    //console.log("Unmounting, removing listener");
    this.focusListener.remove();
  }

  handleFilterInputChange(filterText) {
    this.setState({
			filter: filterText
    });
    //console.log(filterText);
  }

  render() {
    //console.log("card",this.state.cards);
    const { navigation } = this.props;
    return (
      <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
        <View style={styles.container}>
          <Loader
            loading={this.state.loading} />
        </View>

        <NavBar
          title="Task List"
          left={(
            <TouchableOpacity onPress={() => this.props.navigation.openDrawer()}>
              <Icon
                name="menu"
                family="feather"
                size={theme.SIZES.BASE}
                color={theme.COLORS.ICON}
              />
            </TouchableOpacity>
          )}
          style={Platform.OS === 'android' ? { marginTop: theme.SIZES.BASE } : null}
        />

         <ScrollView contentContainerStyle={styles.cards}>
          <Input style={styles.card} label='Search for tasks' onChangeText={this.handleFilterInputChange} right icon='search' family='font-awesome'/>
          <Block flex space="between">
            {this.state.cards && this.state.cards.filter((card, id) => card.title.toUpperCase().includes(this.state.filter.toUpperCase())).map((card, id) => (
              <TouchableOpacity
                        flex
                        borderless
                        key={`card-${card.id}`}
                        onPress={() => card.supervising ? navigation.navigate('TaskSupervisor', {Task: card}) : navigation.navigate('TaskView', {Task: card})}
                        >
                <Block
                  card
                  height={65}
                  shadowColor={theme.COLORS.BLACK}
                  style={styles.card}
                >
                  <Text color={card.supervising ? theme.COLORS.RED : null}>
                  {card.title}
                  </Text>
                  <Text muted>
                  {card.startTime}
                  </Text>
                </Block>
              </TouchableOpacity>
            ))}
          </Block>
        </ScrollView>
      </Block>
    );
  }
}

const styles = StyleSheet.create({
  cards: {
    width,
    backgroundColor: theme.COLORS.WHITE,
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  card: {
    backgroundColor: theme.COLORS.WHITE,
    width: width - theme.SIZES.BASE * 2,
    marginVertical: theme.SIZES.BASE * 0.875,
    padding: 12,
    elevation: theme.SIZES.BASE / 2,
  }
});
