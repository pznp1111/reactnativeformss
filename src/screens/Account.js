import React from 'react';
import {
  Alert, Dimensions, KeyboardAvoidingView, StyleSheet, Platform, AsyncStorage, View, StatusBar, TouchableOpacity
} from 'react-native';

import { Table, Row, Rows, Cell, TableWrapper } from 'react-native-table-component';

// galio component
import {
  Block, Button, Input, NavBar, Text, Icon
} from 'galio-framework';
import theme from '../theme';
import constants from '../constant'

const { height, width } = Dimensions.get('window');
const columnWidths = [width * 0.9 * 0.25, width * 0.9 * 0.75]

// const dummyData = [
//   ["Username", ""],
//   ["Skill", "testFullName"],
//   ["Gender", "Male"],
//   ["Supervisor", "testSupervisor"]
// ];
var infoData = [];

class Account extends React.Component {



  render() {

      var row = ["Username"];
      row.push(global.Detail.UserName);
      infoData.push(row);

      row = ["Skill"];
      var skillstr = "";
      var tempObj = JSON.parse(global.Detail.Skills);
      console.log("skills",tempObj);
      for(var i =0;i<tempObj.length;i++){
        skillstr += tempObj[i].Name + " ";
      }
      row.push(skillstr);
      infoData.push(row);
      // row = ["Gender", ""];
      // infoData.push(row);
      // row = ["Supervisor", ""];
      // infoData.push(row);


    const { navigation } = this.props;
    return (
      <Block safe flex style={{ backgroundColor: theme.COLORS.WHITE }}>
        <NavBar
          title="Account"
          left={(
            <TouchableOpacity onPress={() => navigation.openDrawer()}>
              <Icon
                name="menu"
                family="feather"
                size={theme.SIZES.BASE}
                color={theme.COLORS.ICON}
              />
            </TouchableOpacity>
          )}
          style={Platform.OS === 'android' ? { marginTop: theme.SIZES.BASE } : null}
        />
        <View style={styles.container}>
          <Block style={{width: width * 0.9}}>
            <Table borderStyle={styles.tableBorderStyle}>
              {
                infoData.map((rowData, index) => (
                  <TableWrapper key={index} style={index%2 ? styles.row : styles.alternateRow}>
                    {
                      rowData.map((cellData, cellIndex) => (
                        <Cell key={cellIndex} data={cellData} style={styles.cellStyle} textStyle={cellIndex == 0 ? styles.tableTextStyleColOne : styles.tableTextStyleColTwo} width={columnWidths[cellIndex]} />
                      ))
                    }
                  </TableWrapper>
                ))
              }
            </Table>
          </Block>
          <Button onPress={this._signOutAsync}>Logout</Button>
        </View>
      </Block>
    );
  }
  _signOutAsync = async () => {
    await AsyncStorage.removeItem('access_token');

    this.props.navigation.navigate('Auth');
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: theme.SIZES.BASE * 0.3,
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: theme.COLORS.WHITE,
  },
  row: {
    flexDirection: 'row',
    height: 30,
    backgroundColor: theme.COLORS.WHITE
  },
  alternateRow: {
    flexDirection: 'row',
    height: 30,
    backgroundColor: '#EEEEEE'
  },
  cellStyle: {
    padding: 5
  },
  tableTextStyleColOne: {
    textAlign: 'center'
  },
  tableTextStyleColTwo: {
    textAlign: 'left',
    color: '#666666'
  },
  tableBorderStyle: {
    borderWidth: 1,
    borderColor: '#DDDDDD'
  }
});

export default Account;
