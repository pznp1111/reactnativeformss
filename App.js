import React from 'react';
import {
  Alert, Dimensions, KeyboardAvoidingView, StyleSheet, Platform, View, StatusBar
} from 'react-native';
import GalioApp from './routes';
import { createSwitchNavigator, createAppContainer } from 'react-navigation'
import { createStackNavigator } from 'react-navigation-stack'
import Loading from './src/screens/Loading'
import Login from './src/screens/Login'

// galio component
import {
  Block, Button, Input, NavBar, Text,
} from 'galio-framework';
import theme from './src/theme';
import constants from './src/constant'

const { height, width } = Dimensions.get('window');

const AuthStack = createStackNavigator(
	{ 
		Login: Login 
	}, {
		headerMode: 'none'
	}
);
const SwitchApp = createSwitchNavigator(
  {
    Loading: Loading,
    App: GalioApp,
    Auth: AuthStack
  }, {
    initialRouteName: 'Loading'
  }
);

export default createAppContainer(SwitchApp);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-around',
    paddingTop: theme.SIZES.BASE * 0.3,
    paddingHorizontal: theme.SIZES.BASE,
    backgroundColor: theme.COLORS.WHITE,
  },
  social: {
    width: theme.SIZES.BASE * 3.5,
    height: theme.SIZES.BASE * 3.5,
    borderRadius: theme.SIZES.BASE * 1.75,
    justifyContent: 'center',
  },
});